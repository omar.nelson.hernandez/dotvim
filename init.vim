"-------------------------------------------------------------------------------
" Useful information
"
" convert spaces to tabs first
" set noexpandtab
"set tabstop=4
" set shiftwidth=4
" retab!
" " now you have tabs instead of spaces, so insert spaces according to
" " your new preference
" set tabstop=2
" set shiftwidth=2
" set expandtab
" retab!
" "
"-------------------------------------------------------------------------------

" General options {{{
set nocompatible                                           " Resets all settings, make vim not behave like vi. First line of config file
filetype indent plugin on                                  " Enable automatic indentation, plugins and filetype detection
let mapleader=","                                          " Change the map leader, \ is very hard to press
set wildmenu                                               " Command line completition
set showcmd                                                " Show commands on the last line of the screen
set hlsearch                                               " Set highlight search
set modeline                                             " Disables the line that configures vim per file
set ignorecase                                             " Use case insensitive search, except when using capital letters
set smartcase                                              " Use case insensitive search, except when using capital letters
set backspace=indent,eol,start                             " Backspace over indent, eol and start of insert action
set autoindent                                             " Use indenting from file when no specific indenting is available
set nostartofline                                          " Don't allow commands to return to the start of the line
set ruler                                                  " Display line,col
set laststatus=2                                           " Always show status line
set confirm                                                " Ask for confirmation for unsaved changes
set mouse=                                                " Enable use of mouse for all modes
set cmdheight=2                                            " Avoids "Press <Enter> to continue "
set number                                                 " Display line numbers
set notimeout ttimeout ttimeoutlen=200                     " Timeout on keycodes but not on mappings
set showmode                                               " Show the mode the editor is currently in
set showmatch                                              " Show matching brackets.
set incsearch                                              " Incremental search
set clipboard=unnamedplus
set encoding=utf-8
scriptencoding utf-8
set listchars=eol:↲,trail:·,nbsp:☠,tab:>-,extends:<,precedes:>,conceal:┊
set list                                                   " Mark special characters
set viewoptions=folds,options,cursor,unix,slash            " Better Unix / Windows compatibility
set history=1000                                           " Store a ton of history (default is 20)
set cursorline                                             " Enable the cursor line
set relativenumber                                         " Enable numbers relative to the current line
set virtualedit=block                                      " Enable block editing past line end
set belloff=all
set completeopt=menu,menuone,noselect

" }}}

" Vim Plug {{{

if has("win32")
  let plugPath='~/vimfiles/plugged/'
else
  if has("unix")
    let plugPath='~/.config/nvim/plugged/'
  endif
endif

"""""""""""""""""""""""""""""""""""""""""""""""""" Plug begin
call plug#begin(plugPath)
  Plug 'tpope/vim-dispatch'
" Git integration
  Plug 'tpope/vim-fugitive'

" Finder
"""""""""""""""""""""""""""""""""""""""""""""""""" nvim-telescope/telescope.nvim
  Plug 'nvim-telescope/telescope.nvim'
" --> Neovim 0.5
" --> BurntSushi/ripgrep - finder [package repo]
" --> nvim-telescope/telescope-fzf-native.nvim - native sorter with fzf support
  Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
" --> sharkdp/fd - finder [package repo]
" --> nvim-treesitter/nvim-treesitter - finder/previewer
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" --> neovim LSP - picker
  Plug 'neovim/nvim-lspconfig'
" --> kyazdani42/nvim-web-devicons - icons
  Plug 'kyazdani42/nvim-web-devicons'
" --> plenary - some functions
  Plug 'nvim-lua/plenary.nvim'
" --> nvim-telescope/telescope-file-browser.nvim - file browser replacing :Explore
  Plug 'nvim-telescope/telescope-file-browser.nvim'

" LSP
"""""""""""""""""""""""""""""""""""""""""""""""""" python-lsp/python-lsp-server
" --> pip install 'python-lsp-server[all]'
" --> pip install pyls-flake8
" --> pip install pylsp-mypy
" --> pip install pyls-isort
" --> pip install python-lsp-black
" --> pip install pyls-memestra

"""""""""""""""""""""""""""""""""""""""""""""""""" ansible/ansible-language-server
  Plug 'ansible/ansible-language-server'

" Cheatsheet
"""""""""""""""""""""""""""""""""""""""""""""""""" dbeniamine/cheat.sh-vim
  Plug 'RishabhRD/nvim-cheat.sh'
" --> curl https://cht.sh/:cht.sh > cht.sh
" --> RishabhRD/popfix
  Plug 'RishabhRD/popfix'

" Autocompletion
"""""""""""""""""""""""""""""""""""""""""""""""""" hrsh7th/nvim-cmp
  Plug 'hrsh7th/nvim-cmp'
" --> hrsh7th/cmp-nvim-lsp - Source: LSP
  Plug 'hrsh7th/cmp-nvim-lsp'
" --> hrsh7th/cmp-buffer - Source: buffer
  Plug 'hrsh7th/cmp-buffer'
" --> onsails/lspkind-nvim - Pictograms for autocompletion suggestions
  Plug 'onsails/lspkind-nvim'
" --> hrsh7th/cmp-path
  Plug 'hrsh7th/cmp-path'

" Show hex colors
"""""""""""""""""""""""""""""""""""""""""""""""""" ap/vim-css-color
  Plug 'ap/vim-css-color'

" Themes
""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
  "Plug 'bluz71/vim-nightfly-guicolors'
  " bluz71/vim-moonfly-colors
  " EdenEast/nightfox.nvim
  " NTBBloodbath/doom-one.nvim
  " rafamadriz/neon
  " Shatur/neovim-ayu
  " yonlu/omni.vim
  " Haron-Prime/Antares

" Statusline
"""""""""""""""""""""""""""""""""""""""""""""""""" - hoob3rt/lualine.nvim
" --> kyazdani42/nvim-web-devicons
  Plug 'hoob3rt/lualine.nvim'

" Markdown previewer
  Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' } " Requires nodejs, yarn

"""""""""""""""""""""""""""""""""""""""""""""""""" Maybe?
" kdheepak/tabline.nvim

"""""""""""""""""""""""""""""""""""""""""""""""""" org
Plug 'kristijanhusak/orgmode.nvim'
Plug 'akinsho/org-bullets.nvim'
"Plug 'lukas-reineke/headlines.nvim'
Plug 'dhruvasagar/vim-table-mode'

"""""""""""""""""""""""""""""""""""""""""""""""""" documentation
Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Filetype syntax for kitty
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'fladson/vim-kitty'

call plug#end()
" }}}

" Sourcing {{{

"""""""""""""""""""""""""""""""""""""""""""""""""" Variable setup
if has("win32")
  let homeDir='~/AppData/Local/nvim'
else
  if has("unix")
    let homeDir='~/.config/nvim'
    let g:pythonVenv = "~/coding/"
  endif
endif

"""""""""""""""""""""""""""""""""""""""""""""""""" Load plugin configs
let plug_configs = [
      \ '/plugconf/nvim-web-devicons.lua',
      \ '/plugconf/telescope.lua',
      \ '/plugconf/nvim-cmp.lua',
      \ '/plugconf/nvim-treesitter.lua',
      \ '/plugconf/tokyonight.lua',
      \ '/plugconf/lualine.lua',
      \ '/plugconf/org.lua',
      "\ '/plugconf/headlines.lua',
      \ '/plugconf/nvim-lspconfig.lua',
      \ '/plugconf/vim-doge.vim',
      \ '/plugconf/org-bullets.lua',
      \]

for files in plug_configs
  for f in glob(homeDir.files, 1, 1)
    exec 'source' f
  endfor
endfor

" }}}

" Color scheme settings {{{
syntax on
set termguicolors
colorscheme tokyonight
" }}}

" Wrapping options {{{
set cc=100
set textwidth=100
set wrapmargin=0                                           " Disable line wrapping
set nowrap                                                 " Don't wrap lines, I don't like it
set formatoptions-=ro
set formatoptions+=l
" }}}

" Indentation options {{{
set shiftwidth=2                                           " Size of tab
set softtabstop=2                                          " Size of tab
set tabstop=2                                              " Size of tabs
set expandtab                                              " Tabs expanded to spaces
set smartindent                                            " Smart indent enabled
" }}}

" Split preferences {{{
set splitbelow
set splitright
" }}}


" Setup directories for backup, swap and undo {{{
function! EnsureDirExists( dir )
  if !isdirectory( a:dir )
    if exists( "*mkdir" )
      call mkdir( a:dir, 'p' )
      echo "Created directory: " . a:dir
    else
      echo "Please create directory: " . a:dir
    endif
  endif
endfunction

call EnsureDirExists(expand("~/.vimbackups"))
call EnsureDirExists(expand("~/.vimswap"))
call EnsureDirExists(expand("~/.vimundo"))

if has("win32")
  set backupdir=~/.vimbackups//
  set undodir=~\.vimundo\\
  set directory=~/.vimswap//
else
  if has("unix")
    set backupdir=~/.vimbackups//
    set undodir=~/.vimundo//
    set directory=~/.vimswap//
  endif
endif

set backup
set undofile
set swapfile
" }}}

" Mappings {{{

" Mapping <FX> keys 
" Use F2 to cycle forward through the bookmarks
nmap <F2> ']
" Use F2 to cycle backward through the bookmarks
nmap <S-F2> '[
" Use Ctrl + F2 to toogle a bookmark
nmap <C-F2> m.
" Use <F3> to toggle between 'paste' and 'nopaste'
nnoremap <F3> :set invpaste paste?<CR>
" Obtain the name of the current file
nnoremap <F4> :let @* = expand("%:p")<CR>
"map <F5> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">" . " FG:" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"fg#")<CR>
map <F5> :so $MYVIMRC<CR>
" Beautify line
nnoremap <F8> :call Beautify()<CR>
" Convert all tabs to spaces
nnoremap <F9> :call Tabs2Spaces()<CR>
" Toggle indent guides
nnoremap <F12> <Plug>IndentGuidesToggle
" 
" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-L> :nohl<CR><C-L>

" Block comment (double slash)
nnoremap <C-z> i/**/<ESC>h78i*<ESC>a<CR> * <CR><ESC>77a*<ESC>

" Block comment (Number symbol)
nnoremap <C-x> I#<ESC>79a-<ESC>a<CR># <CR><ESC>I#<ESC>79a-<ESC>k
" }}}

" Functions {{{
function! Beautify()
  " Add space after opening bracket
  s/(\s*/( /eg
  " Add space before closing bracket
  s/\s*)/ )/eg
  " Remove spaces in empty brackets
  s/(\s*)/()/eg
  " Fix spaces around brackets
  "s/\s*(\s*\(.\{-}\)\s*)\s*/( \1 )/eg
  " Fix empty brackets
  s/(\s*)/()/eg
  " Fix space after comma
  s/,\s*/, /eg
  " Fix spaces around operators
  s,\s*\(+\|-\|*\|/\|>\|<\|=\|!\|\)=\s*, \1= ,eg
  " Fix space after opening square bracket
  s/\[\s*/\[ /eg
  " Fix space before closing square bracket
  s/\s*\]/ \]/eg
  " Remove spaces in empty square brackets
  s/\[\s*\]/\[\]/eg
endfunction
function! Tabs2Spaces()
" Convert spaces to tabs first
  set noexpandtab
  set tabstop=4
  set shiftwidth=4
  retab!
" Now you have tabs instead of spaces, so insert spaces according to your new preference
  set tabstop=2
  set shiftwidth=2
  set expandtab
  retab!
" Remove trailing spaces
  s/\s\+$//e
endfunction

" Start a journal entry
function! StartJournalEntry()
  let l:fileHeaderLine = search('^---$')
  let l:journalHeader = ["## " . strftime("%Y-%m-%d %H:%M:%S")]
  call add(l:journalHeader, "")
  call add(l:journalHeader, "")
  call add(l:journalHeader, "")
  call append(l:fileHeaderLine + 1, l:journalHeader)
  call cursor(l:fileHeaderLine + 4, 1)
endfunction

" Start a personal journal entry
function! StartPersonalJournalEntry()
  call StartJournalEntry()
  if has("win32")
    let l:personalJournalSkeleton = "~/vimfiles/templates/personal_journal.skeleton"
  else
    if has("unix")
    let l:personalJournalSkeleton = "~/.vim/templates/personal_journal.skeleton"
    endif
  endif
  let l:fileHeaderLine = search("---")
  call cursor(l:fileHeaderLine + 3, 1)
  normal dd
  call append(l:fileHeaderLine + 3, readfile(expand(l:personalJournalSkeleton)))
endfunction

" }}}

" Text templates {{{
command! InsCodixHeader :read ~/.vim/text-templates/codix-header.txt
command! InsCodixFooter :normal i$Log: $<ESC>
command! InsKshHeader :normal i#!/usr/bin/env ksh<CR><ESC>
" }}}

" Autocommands {{{
" Cannot use automatic removal of trailing spaces in the office because it
" will mess up existing sources
"autocmd BufWritePre * %s/\s\+$//e
" Return to last edit position when opening files
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
" Except when the filetype is a git commit
au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])
au BufEnter *.md exe 'noremap <F5> :!start "C:\Program Files\Mozilla Firefox\firefox.exe" %:p<CR>'
" }}}

" markdown-preview.nvim {{{
let g:mkdp_auto_start = 1
" }}}

if has('nvim')
   au TextYankPost * silent! lua vim.highlight.on_yank {timeout=1000}
 endif
