require('orgmode').setup_ts_grammar()

require('orgmode').setup({
  org_agenda_files = '/storage/notebook/org/*',
  org_default_notes_file = '/storage/notebook/org/refile.org',
  org_todo_keywords = {'TODO(t)', 'BLOCKED(b)', '|', 'DONE(d)'},
  org_time_stamp_rounding_minutes = 1,
  org_priority_default = 'C'
})
