require'lualine'.setup {
  options = {
    theme = 'tokyonight'
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch'},
    lualine_c = {
      {
        'filename',
        path = 1
      }
    },
    lualine_x = {
      {
        'diagnostics',
        sources = {'nvim_diagnostic'},
      },
      {
        'diff'
      },
    },
    lualine_y = {'encoding', 'fileformat', 'filetype'},
    lualine_z = {'progress', 'location'},
  },
  extensions = {'chadtree', 'fugitive', 'fzf', 'nerdtree', 'nvim-tree', 'quickfix'}
}
