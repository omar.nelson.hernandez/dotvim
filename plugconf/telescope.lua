local actions = require("telescope.actions")

require("telescope").setup {
  defaults = {
    vimgrep_arguments = {
      "rg",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
      "--hidden",
      "--ignore-file",
        vim.fn.expand("~/.config/git/gitignore")
    },
    mappings = {
      n = {
        ['<C-d>'] = require('telescope.actions').delete_buffer
      },
      i = {
        ["<A-h>"] = "which_key",
        ['<A-d>'] = require('telescope.actions').delete_buffer
      }
    },
  },
  pickers = {
    find_files = {
      hidden = true,
      find_command = {
        "fd",
        "--hidden",
        "--type",
        "f",
        "--ignore-file",
        vim.fn.expand("~/.config/git/gitignore")
      }
    }
  },
  extensions = {
    file_browser = {
      hidden = true
    }
  }
}
require('telescope').load_extension('fzf')
require('telescope').load_extension('file_browser')

local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "<Leader>ff", "<CMD>lua require('telescope.builtin').find_files()<CR>", opts)
vim.api.nvim_set_keymap("n", "<Leader>fg", "<CMD>lua require('telescope.builtin').live_grep()<CR>", opts)
vim.api.nvim_set_keymap("n", "<Leader>fm", "<CMD>lua require('telescope.builtin').man_pages()<CR>", opts)
vim.api.nvim_set_keymap("n", "<Leader>fb", "<CMD>lua require('telescope').extensions.file_browser.file_browser()<CR>", opts)
vim.api.nvim_set_keymap("n", "<Leader>ft", "<CMD>lua require('telescope.builtin').help_tags()<CR>", opts)
vim.api.nvim_set_keymap("n", "<Leader>fu", "<CMD>lua require('telescope.builtin').buffers()<CR>", opts)
