au BufRead,BufNewFile */roles/*.yml set filetype=yaml.ansible
au BufRead,BufNewFile */roles/*.yaml set filetype=yaml.ansible
